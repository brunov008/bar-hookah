package br.com.project.hookahbar.view.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

abstract class MainPagerTabAdapter(fm : FragmentManager): FragmentPagerAdapter(fm){

    val fragmentList = mutableListOf<Fragment>()
    val titleList = mutableListOf<String>()

    abstract fun addFragment(tabTitle: String?, fragment : Fragment)

    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }
}