package br.com.project.hookahbar.view.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.project.hookahbar.R
import br.com.project.hookahbar.viewmodel.RegisterClientViewModel

class RegisterClientFragment : Fragment() {

    private lateinit var viewModel: RegisterClientViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.register_client_fragment, container, false)

        viewModel = ViewModelProviders.of(this).get(RegisterClientViewModel::class.java)

        return view
    }
}
