package br.com.project.hookahbar

import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import br.com.project.hookahbar.core.ui.BaseActivity
import br.com.project.hookahbar.view.activities.MainActivity
import br.com.project.hookahbar.viewmodel.SplashViewModel
import org.jetbrains.anko.startActivity

class SplashActivity : BaseActivity(){

    private lateinit var splashViewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        splashViewModel = ViewModelProviders.of(this).get(SplashViewModel::class.java)

        Handler().postDelayed({
            startActivity<MainActivity>()
            finish()
        }, 3000)
    }

    override fun getNavigationController(): NavController? {
        return null
    }
}
