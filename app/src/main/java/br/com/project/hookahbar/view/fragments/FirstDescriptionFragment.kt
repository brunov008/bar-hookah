package br.com.project.hookahbar.view.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.project.hookahbar.R
import br.com.project.hookahbar.core.ui.BaseFragment

class FirstDescriptionFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.about_first_page, container, false)

        setFragmentView(view)

        return view
    }
}
