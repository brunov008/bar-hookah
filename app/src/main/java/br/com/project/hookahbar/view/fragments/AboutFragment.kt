package br.com.project.hookahbar.view.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup

import br.com.project.hookahbar.R
import br.com.project.hookahbar.core.ui.BaseFragment
import br.com.project.hookahbar.view.adapters.AboutPagerAdapter
import kotlinx.android.synthetic.main.fragment_about.view.*
import kotlinx.android.synthetic.main.home_fragment.view.*

class AboutFragment : BaseFragment() {

    private lateinit var adapter: AboutPagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_about, container, false)

        setFragmentView(view)

        val viewPager = getFragmentView().viewpagerAbout

        adapter = AboutPagerAdapter(childFragmentManager)
        adapter.addFragment(null, FirstDescriptionFragment())
        adapter.addFragment(null, SecondDescriptionFragment())

        viewPager.adapter = adapter

        viewPager.setOnClickListener { }

        view.tablayoutAbout.setupWithViewPager(viewPager)

        view.rigthButton.setOnClickListener { onRightClick() }

        view.leftButton.setOnClickListener { onLeftClick() }

        return view
    }

    private fun onRightClick() {
        val viewpager = getFragmentView().viewpagerAbout
        if (viewpager.currentItem + 1 == adapter.count) {
            viewpager.currentItem = 0
        } else {
            viewpager.currentItem = viewpager.currentItem + 1
        }
    }

    private fun onLeftClick() {
        val viewpager = getFragmentView().viewpagerAbout
        if (viewpager.currentItem == 0) {
            viewpager.currentItem = adapter.count - 1
        } else {
            viewpager.currentItem = viewpager.currentItem - 1
        }
    }
}
