package br.com.project.hookahbar.view.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PagerAdapter(fragmentManager: FragmentManager) : MainPagerTabAdapter(fragmentManager) {

    override fun getPageTitle(position: Int): CharSequence? {
        return titleList[position]
    }

    override fun addFragment(tabTitle: String?, fragment: Fragment) {
        titleList.add(tabTitle!!)
        fragmentList.add(fragment)
    }
}