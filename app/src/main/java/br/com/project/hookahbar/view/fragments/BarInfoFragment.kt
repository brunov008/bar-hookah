package br.com.project.hookahbar.view.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs

import br.com.project.hookahbar.R
import br.com.project.hookahbar.core.data.models.BarInfo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_bar_info.*
import kotlinx.android.synthetic.main.fragment_bar_info.view.*

class BarInfoFragment : Fragment() {

    private val args: BarInfoFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_bar_info, container, false)

        view.button.setOnClickListener {
            view.content.toggle()
        }

        view.text.text = args.myArg!!.description
        view.hourTextView.text = "Horario de funcionamento: ${args.myArg!!.horarios}"
        view.locationTextView.text = args.myArg!!.local
        view.name.text = args.myArg!!.name
        args.myArg!!.image?.let {
            /*
                TODO set Image using picasso. The image in BarInfo object must be string type returning a url
                Picasso.get()
                .load(args.myArg!!.image)
                .centerCrop()
                .error(R.drawable.user_placeholder_error)
                .into(imageBar)
            */

            view.imageBar.setImageBitmap(args.myArg!!.image)
        }

        //socialMedia
        //cardapioBar

        return view
    }
}
