package br.com.project.hookahbar.core.data.models

import android.os.Parcel
import android.os.Parcelable

data class CardapioBar(
    val listaEssencias : ArrayList<String>,
    val listaBebidas : ArrayList<String>
) : Parcelable{

    constructor(parcel: Parcel) : this(
        parcel.createStringArrayList(),
        parcel.createStringArrayList()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest!!.writeArray(listaEssencias.toArray())
        dest.writeArray(listaBebidas.toArray())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CardapioBar> {
        override fun createFromParcel(parcel: Parcel): CardapioBar {
            return CardapioBar(parcel)
        }

        override fun newArray(size: Int): Array<CardapioBar?> {
            return arrayOfNulls(size)
        }
    }

}