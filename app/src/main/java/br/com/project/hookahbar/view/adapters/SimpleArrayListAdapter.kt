package br.com.project.hookahbar.view.adapters

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import android.view.ViewGroup
import gr.escsoft.michaelprimez.revealedittext.tools.UITools;
import android.widget.*
import gr.escsoft.michaelprimez.searchablespinner.interfaces.ISpinnerSelectedView
import br.com.project.hookahbar.R
import br.com.project.hookahbar.core.data.models.BarInfo
import java.lang.Exception


class SimpleArrayListAdapter(private val mContext: Context, list: ArrayList<BarInfo>) :
    ArrayAdapter<String>(mContext, R.layout.view_list_item), Filterable, ISpinnerSelectedView {

    private var mFilteredData: ArrayList<String>
    private var listBarInfo: ArrayList<BarInfo> = list

    private val mStringFilter = StringFilter()
    private lateinit var view: View

    init {
        val listNames = ArrayList<String>()
        list.forEach {
            listNames.add(it.name)
        }
        mFilteredData = listNames
    }

    override fun getCount(): Int {
        return mFilteredData.size + 1
    }

    override fun getItem(position: Int): String? {
        return mFilteredData[position - 1]
    }

    override fun getItemId(position: Int): Long {
        return mFilteredData[position].hashCode().toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return initView(position)
    }

    override fun getSelectedView(position: Int): View {
        return initView(position)
    }

    override fun getNoSelectionView(): View {
        return View.inflate(mContext, R.layout.view_list_no_selection_item, null)
    }

    private fun initView(position: Int) : View{
        if (position == 0) {
            view = noSelectionView
        } else {
            view = View.inflate(mContext, R.layout.view_list_item, null)
            val letters = view.findViewById(R.id.ImgVw_Letters) as ImageView
            val dispalyName = view.findViewById(R.id.TxtVw_DisplayName) as TextView
            letters.setImageDrawable(getTextDrawable(listBarInfo[position - 1].name))
            dispalyName.text = listBarInfo[position - 1].name
        }
        return view
    }

    fun getListPosition(position: Int): BarInfo? {
        var barInfo: BarInfo? = null
        for (value in listBarInfo) {
            if (value.name == mFilteredData[position]) {
                barInfo = value
                break
            }
        }

        return barInfo
    }

    private fun getTextDrawable(displayName: String): TextDrawable? {
        val drawable: TextDrawable?
        if (!TextUtils.isEmpty(displayName)) {
            val color2 = ColorGenerator.MATERIAL.getColor(displayName)
            drawable = TextDrawable.builder()
                .beginConfig()
                .width(UITools.dpToPx(mContext, 32.toFloat()))
                .height(UITools.dpToPx(mContext, 32.toFloat()))
                .textColor(Color.WHITE)
                .toUpperCase()
                .endConfig()
                .round()
                .build(displayName.substring(0, 1), color2)
        } else {
            drawable = TextDrawable.builder()
                .beginConfig()
                .width(UITools.dpToPx(mContext, 32.toFloat()))
                .height(UITools.dpToPx(mContext, 32.toFloat()))
                .endConfig()
                .round()
                .build("?", Color.GRAY)
        }
        return drawable
    }

    override fun getFilter(): Filter {
        return mStringFilter
    }

    inner class StringFilter : Filter() {

        override fun performFiltering(constraint: CharSequence): FilterResults {
            val filterString = constraint.toString().toLowerCase()
            val filterResults = FilterResults()

            val nList = ArrayList<String>(listBarInfo.size)
            var filterableString: String

            listBarInfo.forEach {
                filterableString = it.name
                if (filterableString.toLowerCase().contains(filterString)) {
                    nList.add(filterString)
                }
            }
            filterResults.values = nList
            filterResults.count = nList.size


            return filterResults
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            if (!(results.values as ArrayList<*>).contains("")){
                mFilteredData = results.values as ArrayList<String>
                notifyDataSetChanged()
            }
        }
    }
}