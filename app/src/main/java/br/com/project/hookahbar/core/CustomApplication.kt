package br.com.project.hookahbar.core

import android.app.Application

class CustomApplication : Application() {

    companion object {
        lateinit var context: CustomApplication

        fun getApplicationContext(): CustomApplication {
            return context
        }
    }

    override fun onCreate() {
        super.onCreate()

        context = this
    }
}