package br.com.project.hookahbar.view.activities

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import br.com.project.hookahbar.R
import br.com.project.hookahbar.core.ui.BaseActivity
import br.com.project.hookahbar.viewmodel.MainViewModel
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import org.koin.android.architecture.ext.viewModel

class MainActivity : BaseActivity() {

    //private val mMainViewModel by viewModel<MainViewModel>()

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        configNavigationController()
    }

    private fun configNavigationController() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)

        NavigationUI.setupWithNavController(navigationView, navController)

        navigationView.setNavigationItemSelectedListener(object :
            NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
                menuItem.isChecked = true

                drawerLayout.closeDrawers()

                when (menuItem.itemId) {
                    R.id.client_add -> {
                        navController.navigate(R.id.registerClientFragment)
                    }

                    R.id.local_add -> {
                        navController.navigate(R.id.registerLocalFragment)
                    }

                    R.id.about -> {
                        navController.navigate(R.id.aboutFragment)
                    }
                }
                return true
            }

        })
    }

    override fun getNavigationController(): NavController? {
        return navController
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(Navigation.findNavController(this, R.id.nav_host_fragment), drawerLayout)
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}
