package br.com.project.hookahbar.core.utils;

import android.util.Log;

import java.io.*;

final class FileUtils {

    private static final String TAG = FileUtils.class.getName();

    static String toString(InputStream inputStream, String charset) throws IOException {
        byte[] bytes = toBytes(inputStream);
        return new String(bytes, charset);
    }

    private static byte[] toBytes(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                byteArrayOutputStream.write(buffer, 0, len);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return new byte[0];
        } finally {
            try {
                byteArrayOutputStream.close();
                inputStream.close();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
    }
}
