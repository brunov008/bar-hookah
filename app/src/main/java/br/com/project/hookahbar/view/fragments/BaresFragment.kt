package br.com.project.hookahbar.view.fragments


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.project.hookahbar.R
import br.com.project.hookahbar.view.adapters.SimpleArrayListAdapter
import gr.escsoft.michaelprimez.searchablespinner.interfaces.IStatusListener
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.project.hookahbar.core.ui.BaseFragment
import br.com.project.hookahbar.core.data.models.BarInfo
import br.com.project.hookahbar.viewmodel.BaresViewmodel
import gr.escsoft.michaelprimez.searchablespinner.interfaces.OnItemSelectedListener
import kotlinx.android.synthetic.main.fragment_bares.view.*

class BaresFragment : BaseFragment() {

    private lateinit var viewModel: BaresViewmodel

    private var mSimpleArrayListAdapter: SimpleArrayListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_bares, container, false)

        setFragmentView(view)

        viewModel = ViewModelProviders.of(this).get(BaresViewmodel::class.java)

        return view
    }

    override fun onStart() {
        super.onStart()

        showLoading()

        viewModel.getListBarInService().observe(this,
            Observer { list ->
                hideLoading()
                list?.let {
                    mockImages(list)  //FIXME Mock Images

                    setupSpinner(list)
                }
            }
        )
    }

    private fun mockImages(list: List<BarInfo>) {
        list.map {
            when (it.name) {
                "Agrabah" -> it.image = getBitmap(R.drawable.agrabah)
                "Essence Lounge Bar" -> it.image = getBitmap(R.drawable.essence)
                "Shisha" -> it.image = getBitmap(R.drawable.shisha)
                "Hookah" -> it.image = getBitmap(R.drawable.hookah)
                else -> it.image = null
            }
        }
    }

    private fun getBitmap(id: Int): Bitmap {
        return BitmapFactory.decodeResource(resources, id)
    }

    private fun setupSpinner(list: List<BarInfo>?) {
        val view = getFragmentView()

        mSimpleArrayListAdapter = SimpleArrayListAdapter(view.context, list as ArrayList<BarInfo>)

        view.spinner.setAdapter(mSimpleArrayListAdapter)
        view.spinner.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(view: View?, position: Int, id: Long) {
                val barInfo = mSimpleArrayListAdapter!!.getListPosition(position - 1)
                barInfo?.let {
                    view?.spinner?.hideEdit()
                    goToBarInfoFragment(barInfo)
                }
            }

            override fun onNothingSelected() {
                Toast.makeText(context, "Nothing Selected", Toast.LENGTH_SHORT).show()
            }

        })
        view.spinner.setStatusListener(object : IStatusListener {
            override fun spinnerIsOpening() {
                //context?.toast("Spinner is opening")
            }

            override fun spinnerIsClosing() {
                //context?.toast("Spinner is closing\"")
            }
        })
    }

    private fun goToBarInfoFragment(arg: BarInfo) {
        //Handler used to wait the spinner animation
        Handler().postDelayed({
            val action = HomeFragmentDirections.actionHomeFragmentToBarInfoFragment(arg)

            getNavigationController()?.navigate(action)
        }, 500)
    }
}
