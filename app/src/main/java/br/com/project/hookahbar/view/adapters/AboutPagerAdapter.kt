package br.com.project.hookahbar.view.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class AboutPagerAdapter(fm: FragmentManager) : MainPagerTabAdapter(fm) {

    override fun addFragment(tabTitle: String?, fragment: Fragment) {
        fragmentList.add(fragment)
    }
}