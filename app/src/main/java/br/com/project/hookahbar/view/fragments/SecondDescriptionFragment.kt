package br.com.project.hookahbar.view.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.project.hookahbar.R
import kotlinx.android.synthetic.main.fragment_second_description.view.*
import android.text.Html
import br.com.project.hookahbar.core.ui.BaseFragment
import org.jetbrains.anko.toast


class SecondDescriptionFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_second_description, container, false)

        view.about_like_text.text = Html.fromHtml(context!!.getString(R.string.about_text_description))

        view.emailButton.setOnClickListener {
            view.context.toast("on Email Click")
        }

        return view
    }
}
