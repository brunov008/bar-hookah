package br.com.project.hookahbar.core.ui

import androidx.lifecycle.ViewModel
import java.lang.ref.WeakReference

abstract class BaseModel: ViewModel()