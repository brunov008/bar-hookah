package br.com.project.hookahbar.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.project.hookahbar.R
import br.com.project.hookahbar.viewmodel.RegisterLocalViewModel
import org.koin.android.architecture.ext.viewModel

class RegisterLocalFragment : Fragment() {

    //private val viewModel by viewModel<RegisterLocalViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.register_local_fragment, container, false)

        return view
    }
}
