package br.com.project.hookahbar.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import br.com.project.hookahbar.R
import br.com.project.hookahbar.view.adapters.PagerAdapter
import kotlinx.android.synthetic.main.home_fragment.view.*

class HomeFragment : Fragment() {

    //private val viewModel by viewModel<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_fragment, container, false)

        setupViewPager(view)

        return view
    }

    private fun setupViewPager(view : View){
        val customAdapter = PagerAdapter(childFragmentManager)
        customAdapter.addFragment("Bares", BaresFragment())
        customAdapter.addFragment("Promoções", PromoFragment())

        view.viewpager.adapter = customAdapter
        view.tablayout.setupWithViewPager(view.viewpager)
    }
}
