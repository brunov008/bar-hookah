package br.com.project.hookahbar.core.data.models

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BarInfo(
    val name : String,
    var image : Bitmap?,
    val description : String,
    val local : String,
    val horarios: String,
    val cardapioBar: CardapioBar?,
    val socialMedia : String?,
    val location : String?
) : Parcelable