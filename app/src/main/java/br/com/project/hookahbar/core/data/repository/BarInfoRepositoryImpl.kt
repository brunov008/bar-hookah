package br.com.project.hookahbar.core.data.repository

import android.os.Handler
import android.os.HandlerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.project.hookahbar.core.data.models.BarInfo

class BarInfoRepositoryImpl : BarInfoRepository{

    override fun getListBarInfo(): LiveData<List<BarInfo>?> {

        val data = MutableLiveData<List<BarInfo>?>()

        val handlerThread = HandlerThread("HandlerThread")
        handlerThread.start()
        val handler = Handler(handlerThread.looper)

        handler.postDelayed({
            val list = mutableListOf<BarInfo>()
            val barInfo = BarInfo(
                "Agrabah",
                null,
                "LorenIpSEUNDUWIDNJAWKDNJWKADNJWKAXKW",
                "W3 norte", "13h as 22h",
                null,
                null,
                "Asa norte"
            )

            val barInfo1 = BarInfo(
                "Essence Lounge Bar",
                null,
                "dnajwkxnjakwnxjkawdnjwadccccccc",
                "W3 norte", "13h as 22h",
                null,
                null,
                "Asa norte"
            )
            val barInfo2 = BarInfo(
                "Shisha",
                null,
                "dwkmalxmkwlaxmkwldnjakvda",
                "L2 norte", "14h as 21h",
                null,
                null,
                "Asa norte"
            )
            val barInfo3 = BarInfo(
                "Hookah",
                null,
                "damwklxamkwlmwakldnklanekfla",
                "Perto do Iesb", "13h as 22h",
                null,
                null,
                "Aguas Claras"
            )

            list.add(barInfo)
            list.add(barInfo1)
            list.add(barInfo2)
            list.add(barInfo3)

            data.postValue(list)
        }, 8000)

        return data
    }
}