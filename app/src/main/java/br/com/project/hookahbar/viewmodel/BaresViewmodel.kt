package br.com.project.hookahbar.viewmodel

import androidx.lifecycle.LiveData
import br.com.project.hookahbar.core.ui.BaseModel
import br.com.project.hookahbar.core.data.models.BarInfo
import br.com.project.hookahbar.core.data.repository.BarInfoRepository
import br.com.project.hookahbar.core.data.repository.BarInfoRepositoryImpl

class BaresViewmodel(private val repository: BarInfoRepository = BarInfoRepositoryImpl()) : BaseModel(){

    fun getListBarInService() : LiveData<List<BarInfo>?>{
        return repository.getListBarInfo()
    }
}