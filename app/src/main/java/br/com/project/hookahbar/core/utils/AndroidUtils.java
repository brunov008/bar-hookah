package br.com.project.hookahbar.core.utils;

import android.content.Context;
import android.content.res.Resources;

import java.io.IOException;
import java.io.InputStream;

public final class AndroidUtils {

    public static String readRawFileString(Context context, int raw, String charset) throws IOException {
        Resources resources = context.getResources();
        InputStream inputStream = resources.openRawResource(raw);
        return FileUtils.toString(inputStream, charset);
    }
}
