package br.com.project.hookahbar.core.data.repository

import androidx.lifecycle.LiveData
import br.com.project.hookahbar.core.data.models.BarInfo

interface BarInfoRepository {
    fun getListBarInfo() : LiveData<List<BarInfo>?>
}