package br.com.project.hookahbar.core.ui

import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import kotlinx.android.synthetic.main.fragment_bares.view.*

abstract class BaseFragment : Fragment(){

    private lateinit var fragmentView: View

    fun getNavigationController() : NavController?{
        return (context as BaseActivity).getNavigationController()
    }

    fun setFragmentView(view: View){
        fragmentView = view
    }

    fun getFragmentView() = fragmentView

    fun showLoading() {
        getFragmentView().progressBar.visibility = View.VISIBLE
    }

    fun hideLoading() {
        getFragmentView().progressBar.visibility = View.GONE
    }

}